var angular = angular;

/**
 * @ngdoc overview
 * @name chatClientApp
 * @description
 * # chatClientApp
 *
 * Main module of the application.
 */
angular
  .module('ChatApp', [
    'ngAnimate',
//    'ngCookies',
//    'ngResource',
    'ngRoute',
//    'ngSanitize',
//    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'templates/main.html',
        controller: 'MainCtrl'
      })
      .when('/about', {
        templateUrl: 'templates/twitter.html',
        controller: 'TwitterCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
