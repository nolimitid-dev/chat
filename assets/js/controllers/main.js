var angular = angular,
    io = io,
    $ = $,
    _ = _,
    console = console;

angular.module('ChatApp')
//.service('userModel', function(){
//    if(angular.isDefined(localStorage.userModel)) {
//        var model = JSON.parse(localStorage.userModel);
//        this.name = model.name;
//    }
//    else {
//        this.name = null;
//    }
//    return this;
//})
.value('messages', [])
.service('chatListener', function(socket, messages){
    socket.on('chat message', function(msg){
        messages.push(JSON.parse(msg));
        console.log(messages);
    });
})
.controller('MainCtrl', function ($scope, $rootScope, messages, chatListener) {
    $scope.user = {
        rooms: [
            {
                name: 'public',
                state: true
            }
        ],
        message : ''
    };
    $scope.messages = messages;
    $scope.user.sendTo = $scope.user.rooms[0];
//    $scope.tempData = {
//        addRoom : null
//    };
    $scope.send = function(){
        io.socket.emit('chat message', JSON.stringify($scope.user));
        $scope.user.message = '';
    };
//    $scope.messages = messages;
//    $scope.add = function(){
//        $scope.messages.push('awd');
//    };
//    $scope.joinRoom = function(){
//        $scope.user.rooms.push({
//            name: $scope.tempData.addRoom,
//            state: true
//        });
//        socket.emit('join room', $scope.tempData.addRoom);
//        $scope.tempData.addRoom = null;
//    };
    $scope.leaveRoom = function(index){
        console.log('sek talah');
//        socket.emit('leave room', $scope.user.rooms[index].name);
//        $scope.user.rooms.splice(index, 1);
    };
});